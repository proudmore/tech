<?php

namespace common\models;

use dektrium\user\models\User as BaseUser;
use yii\web\IdentityInterface;
use app\models\Employee;

/**
 * User model
 *
 * @property string $humanName Return field 'name' from profile
 *
 * @property \dektrium\rbac\models\AuthItem[] $roles
 * @property \dektrium\rbac\models\AuthItem[] $permissions
 *
 */

class User extends BaseUser implements IdentityInterface{

    public function getRoles() {
        return \Yii::$app->authManager->getRolesByUser($this->id);
    }

    public function getPermissions() {
        return \Yii::$app->authManager->getPermissionsByUser($this->id);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['user_id' => 'id']);
    }

}