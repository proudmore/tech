<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@employee', dirname(dirname(__DIR__)) . '/employee');
Yii::setAlias('@admin', dirname(dirname(__DIR__)) . '/admin');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
