<?php

use yii\db\Migration;

class m170512_145439_move_column_expected_cost_from_consumption_to_voyage extends Migration
{

    public function safeUp()
    {
        $this->dropColumn('consumption' ,'expected_cost');
        $this->addColumn('voyage', 'expected_cost', $this->integer()->notNull());
    }

    public function safeDown()
    {
        $this->dropColumn('voyage' ,'expected_cost');
        $this->addColumn('consumption', 'expected_cost', $this->integer()->notNull());
    }

}
