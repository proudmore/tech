<?php

use yii\db\Migration;

class m170505_043943_create_table_employee extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('employee', [
            'id' => $this->primaryKey(),
            'department_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'is_free' => $this->boolean()->notNull()->defaultValue(true)
        ], $tableOptions);

        $this->createIndex('is_free', 'employee', 'is_free');

        $this->addForeignKey('department_id', 'employee', 'department_id', 'department', 'id');

        $this->addForeignKey('user_id', 'employee', 'user_id', 'user', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('user_id', 'employee');
        $this->dropForeignKey('department_id', 'employee');
        $this->dropIndex('is_free', 'employee');
        $this->dropTable('employee');
    }

}
