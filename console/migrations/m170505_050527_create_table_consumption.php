<?php

use yii\db\Migration;

class m170505_050527_create_table_consumption extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        $this->createTable('consumption', [
            'id' => $this->primaryKey(),
            'article' => $this->string()->notNull(),
            'voyage_id' => $this->integer()->notNull(),
            'expected_cost' => $this->integer()->notNull(),
            'actual_cost' => $this->integer()->notNull()
        ], $tableOptions);

        $this->addForeignKey('voyage_id', 'consumption', 'voyage_id', 'voyage', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('voyage_id', 'consumption');
        $this->dropTable('consumption');
    }

}