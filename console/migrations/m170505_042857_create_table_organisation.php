<?php

use yii\db\Migration;

class m170505_042857_create_table_organisation extends Migration
{

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        $this->createTable('organisation', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'city_id' => $this->integer()->notNull()
        ], $tableOptions);

        $this->addForeignKey('city_id', 'organisation', 'city_id', 'city', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('city_id', 'organisation');
        $this->dropTable('organisation');
    }

}
