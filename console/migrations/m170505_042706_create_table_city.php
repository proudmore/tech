<?php

use yii\db\Migration;

class m170505_042706_create_table_city extends Migration
{

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        $this->createTable('city', [
            'id' => $this->primaryKey(),
            'name' => $this->string()
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('city');
    }

}



