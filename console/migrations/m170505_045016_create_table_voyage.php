<?php

use yii\db\Migration;

class m170505_045016_create_table_voyage extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        $this->createTable('voyage', [
            'id' => $this->primaryKey(),
            'employee_id' => $this->integer()->notNull(),
            'organisation_id' => $this->integer()->notNull(),
            'date_start' => $this->date()->notNull(),
            'date_end' => $this->date(),
            'is_finished' => $this->boolean()->defaultValue(false)
        ], $tableOptions);

        $this->addForeignKey('employee_id', 'voyage', 'employee_id', 'employee', 'id');
        $this->addForeignKey('organisation_id', 'voyage', 'organisation_id', 'organisation', 'id');

        $this->createIndex('is_finished', 'voyage', 'is_finished');
    }

    public function safeDown()
    {
        $this->dropIndex('is_finished', 'voyage');
        $this->dropForeignKey('organisation_id', 'voyage');
        $this->dropForeignKey('employee_id', 'voyage');
        $this->dropTable('voyage');
    }

}