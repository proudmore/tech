<?php

namespace admin\widgets\switchbtn;

use yii\web\AssetBundle;
use yii\web\View;


class SwitchBtnAsset extends AssetBundle {

	public $sourcePath		 = '@app/widgets/switchbtn/assets';
	public $jsOptions		 = [
		'position' => View::POS_END,
	];
	public $publishOptions	 = [
		'forceCopy' => YII_DEBUG,
	];
	public $depends = [
		'yii\web\JqueryAsset',
	];
	public $js = [
		'switchbtn.js',
	];
	public $css = [
		'switchbtn.css',
	];

}
