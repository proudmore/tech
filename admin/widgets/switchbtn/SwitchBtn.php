<?php

namespace admin\widgets\switchbtn;

use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\View;
use yii\widgets\InputWidget;


class SwitchBtn extends InputWidget {

	private $_id;

	public $value = -1;
	public $options = [];

	protected $defaultOptions = [
		'inputId' => '',
		'onText' => 'On',
		'offText' => 'Off',
	];

	public function init() {
		parent::init();

		$this->_id = 'switchbtn_' . substr(md5(microtime(true)), 0, 8);

		if ( $this->hasModel() ) {
			$this->value = $this->model->{$this->attribute};
		}

		$this->options = array_merge($this->defaultOptions, $this->options);
	}

	public function run() {
		echo Html::beginTag('div');

		if ( !empty($this->options['offText']) ) {
			echo Html::tag('div', $this->options['offText'], [
				'class' => 'switch-option-left',
			]);
		}

		echo Html::beginTag('div', [
			'id' => $this->_id,
			'class' => 'toggle-switch-container',
			'value' => intval($this->value),
		]);

		echo Html::tag('div', '', ['class' => 'toggle-switch animate-bg']);
		echo Html::tag('div', '', ['class' => 'toggle-btn animate-btn']);

		echo Html::endTag('div');

		if ( !empty($this->options['onText']) ) {
			echo Html::tag('div', $this->options['onText'], [
				'class' => 'switch-option-right',
			]);
		}

		if ( $this->hasModel() ) {

			$inputName = Html::getInputName($this->model, $this->attribute);
			$inputId = Html::getInputId($this->model, $this->attribute);
		} else {
			$inputName = $this->name;
			$inputId = 'switchbtn_' . $this->name;
		}

		$this->options['inputId'] = $inputId;

		echo Html::input('hidden', $inputName, intval($this->value), ['id' => $inputId]);

		echo Html::endTag('div');

		$this->registerJs();
	}

	protected function registerJs() {
		SwitchBtnAsset::register($this->view);

		$options = Json::encode($this->options);

		$this->view->registerJs("var {$this->_id} = $options", View::POS_HEAD);
	}

}
