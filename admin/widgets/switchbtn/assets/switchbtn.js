/*
    Created on : 30.08.2016, 11:12:17
    Author     : Inka
*/
$(document.body).on('click', '.toggle-switch-container', function () {
	var options = window[$(this).attr('id')];

	switch( parseInt($(this).attr('value')) ) {
		case -1:
		case 0:
			$(this).attr('value', 1);
			$('#' + options.inputId).val(1);
			if ( typeof options.pluginEvents.switchChange === 'function' ) {
				options.pluginEvents.switchChange(1);
			}
		break;
		case 1:
			$(this).attr('value', 0);
			$('#' + options.inputId).val(0);
			if ( typeof options.pluginEvents.switchChange === 'function' ) {
				options.pluginEvents.switchChange(0);
			}
		break;
	}
});


