<?php

namespace app\models;

use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Linkable;

/**
 * This is the model class for table "voyage".
 *
 * @property integer $id
 * @property integer $employee_id
 * @property integer $organisation_id
 * @property string $date_start
 * @property string $date_end
 * @property boolean $is_finished
 * @property integer $expected_cost
 *
 * @property Consumption[] $consumptions
 * @property Employee $employee
 * @property Organisation $organisation
 */
class Voyage extends \yii\db\ActiveRecord implements Linkable
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'voyage';
    }

    const SCENARIO_SET_END = 'set_end';
    const SCENARIO_CHANGE_EMPLOYEE = 'change_employee';
    const SCENARIO_CHANGE_DESTINATION = 'change_destination';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['employee_id', 'organisation_id', 'date_start', 'expected_cost'], 'required'],
            [['employee_id', 'organisation_id', 'expected_cost'], 'integer'],
            [['is_finished'], 'boolean'],
            [['date_start', 'date_end'], 'date', 'format' => 'YYYY-MM-DD'],
            [['employee_id'], 'exist', 'skipOnError' => false, 'targetClass' => Employee::className(), 'targetAttribute' => ['employee_id' => 'id']],
            [['organisation_id'], 'exist', 'skipOnError' => false, 'targetClass' => Organisation::className(), 'targetAttribute' => ['organisation_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'employee_id' => 'Работник',
            'organisation_id' => 'Организация',
            'date_start' => 'Дата начала',
            'date_end' => 'Дата окончания',
            'is_finished' => 'Завершено',
            'expected_cost' => 'Ожидаемые расходы'
        ];
    }

    public function scenarios()
    {
        return ArrayHelper::merge(parent::scenarios(), [
            self::SCENARIO_SET_END => ['date_end', 'is_finished'],
            self::SCENARIO_CHANGE_EMPLOYEE => ['employee_id'],
            self::SCENARIO_CHANGE_DESTINATION => ['organisation_id']
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConsumptions()
    {
        return $this->hasMany(Consumption::className(), ['voyage_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'employee_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrganisation()
    {
        return $this->hasOne(Organisation::className(), ['id' => 'organisation_id']);
    }

    /**
     * @inheritdoc
     * @return \app\models\queries\VoyageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\queries\VoyageQuery(get_called_class());
    }

    public function fields(){
        return [
            'id' => 'id',
            'organisation' => function($model, $field){
                return $model->organisation->name;
            },
            'date_start' => 'date_start',
            'is_finished' => 'is_finished',
            'consumptions' => 'consumptions'
        ];
    }


    public function getLinks()
    {
        return [
            'self' => Url::to(['voyage/view', 'id' => $this->id], true)
        ];
    }
}
