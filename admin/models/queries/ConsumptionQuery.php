<?php

namespace app\models\queries;

/**
 * This is the ActiveQuery class for [[\app\models\Consumption]].
 *
 * @see \app\models\Consumption
 */
class ConsumptionQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \app\models\Consumption[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\models\Consumption|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function byVoyage($voyage_id){
        return $this->andWhere(['voyage_id' => $voyage_id]);
    }
}
