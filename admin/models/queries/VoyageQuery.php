<?php

namespace app\models\queries;

/**
 * This is the ActiveQuery class for [[\app\models\Voyage]].
 *
 * @see \app\models\Voyage
 */
class VoyageQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \app\models\Voyage[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\models\Voyage|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function byCurrentUser(){
        return $this->andWhere(['employee_id' => \Yii::$app->user->identity->employee->id]);
    }
}
