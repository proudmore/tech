<?php

namespace app\models;

use common\models\User;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "employee".
 *
 * @property integer $id
 * @property integer $department_id
 * @property integer $user_id
 * @property boolean $is_free
 *
 * @property Department $department
 * @property User $user
 * @property Voyage[] $voyages
 */
class Employee extends \yii\db\ActiveRecord
{

    const SCENARIO_CREATE = 'create';

    const STATUS_FREE = 1;
    const STATUS_BUSY = 0;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'employee';
    }


    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return ArrayHelper::merge(parent::scenarios(),[
            self::SCENARIO_CREATE => ['department_id'],
        ]);

    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['department_id', 'user_id'], 'required'],
            [['department_id', 'user_id'], 'integer'],
            [['is_free'], 'boolean'],
            [['department_id'], 'exist', 'skipOnError' => true, 'targetClass' => Department::className(), 'targetAttribute' => ['department_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'department_id' => 'Отдел',
            'user_id' => 'User ID',
            'is_free' => 'Свободен',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartment()
    {
        return $this->hasOne(Department::className(), ['id' => 'department_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVoyages()
    {
        return $this->hasMany(Voyage::className(), ['employee_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \app\models\queries\EmployeeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\queries\EmployeeQuery(get_called_class());
    }

    /**
     * returns an Employers with a FREE status
     * @return Employee[]|array
     */
    public static function getFreeEmployers(){
        $ids = \Yii::$app->authManager->getUserIdsByRole('employee');
        $users = Employee::find()
            ->where(['user_id' => $ids])
            ->andWhere(['is_free' => Employee::STATUS_FREE])
            ->all();
        return $users;
    }

    public function setBusy(){
        $this->is_free = self::STATUS_BUSY;
        $this->save();
    }

    public function setFree(){
        $this->is_free = self::STATUS_FREE;
        $this->save();
    }
}
