<?php

namespace app\models;

use yii\web\Linkable;
use yii\helpers\Url;

/**
 * This is the model class for table "consumption".
 *
 * @property integer $id
 * @property string $article
 * @property integer $voyage_id
 * @property integer $actual_cost
 *
 * @property Voyage $voyage
 */
class Consumption extends \yii\db\ActiveRecord implements Linkable
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'consumption';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['article', 'voyage_id', 'actual_cost'], 'required'],
            [['voyage_id', 'actual_cost'], 'integer'],
            [['article'], 'string', 'max' => 255],
            [['voyage_id'], 'exist', 'skipOnError' => false, 'targetClass' => Voyage::className(), 'targetAttribute' => ['voyage_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'article' => 'Статья',
            'voyage_id' => 'Voyage ID',
            'actual_cost' => 'Расходы',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVoyage()
    {
        return $this->hasOne(Voyage::className(), ['id' => 'voyage_id']);
    }

    /**
     * @inheritdoc
     * @return \app\models\queries\ConsumptionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\queries\ConsumptionQuery(get_called_class());
    }

//    public function fields()
//    {
//        return [
//            'id' => 'id',
//            'article' => 'article',
//            'value' => 'actual_cost'
//
//        ];
//    }

    public function getLinks()
    {
        return [
            'self' => Url::to(['consumptions/view', 'id' => $this->id], true),
        ];
    }
}
