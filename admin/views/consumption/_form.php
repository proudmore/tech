<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Consumption */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="consumption-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'voyage_id')->widget(Select2::className(), [
        'data' => ArrayHelper::map($voyages, 'id', 'id')
    ]) ?>

    <?= $form->field($model, 'article')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'expected_cost')->textInput() ?>

    <?= $form->field($model, 'actual_cost')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
