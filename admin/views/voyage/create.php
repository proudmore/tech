<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Voyage */

$this->title = Yii::t('app', 'Создать Командировку');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Командировки'), 'url' => ['index']];
?>
<div class="voyage-create">

    <?= $this->render('_form', [
        'model' => $model,
        'users' => $users,
        'organisations' => $organisations
    ]) ?>

</div>
