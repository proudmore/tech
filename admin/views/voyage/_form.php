<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use admin\widgets\SwitchBtn;


/* @var $this yii\web\View */
/* @var $model app\models\Voyage */
/* @var $form yii\widgets\ActiveForm */
/* @var $users \common\models\User[] */
/* @var $organisations \app\models\Organisation[] */
?>

<div class="voyage-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'employee_id')->widget(Select2::className(), [
            'data' => ArrayHelper::map($users, 'id', 'name'),
            'language' => 'ru'

    ]) ?>

    <?= $form->field($model, 'organisation_id')->widget(Select2::className(), [
            'data' => ArrayHelper::map($organisations, 'id', 'name'),
    ]) ?>

    <?= $form->field($model, 'expected_cost'); ?>

    <?= $form->field($model, 'date_start')->widget(DatePicker::className(), [
        'removeButton' => false,
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'yyyy-mm-dd',
            'startDate' => date('Y-m-d')
        ]
    ])  ?>

    <?= $form->field($model, 'date_end')->widget(DatePicker::className(), [
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'yyyy-mm-dd'
        ]
    ]) ?>

    <?= $form->field($model, 'is_finished')->widget(SwitchBtn::className(), [
        'options' => [
            'onText' => 'Завершено',
            'offText' => '',
        ],
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
