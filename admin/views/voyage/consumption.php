<?php

use yii\grid\GridView;
use yii\helpers\Html;

$this->title = 'Расходы';
$this->params['breadcrumbs'][] = ['label' => 'Командировки', 'url' => ['/voyage']];
$this->params['breadcrumbs'][] = $voyage_id;
$this->params['breadcrumbs'][] = $this->title;

/* @var $this \yii\web\View */

?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'article',
        'actual_cost',
    ]
]);


