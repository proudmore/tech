<?php

use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\bootstrap\Html;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Командировки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="voyage-index">
    <p>
        <a class="btn btn-success" href="<?= Url::to(['/voyage/create']) ?>"><span class="glyphicon glyphicon-plus"></span></a>
    </p>

<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            [
                'attribute' => 'employee.user.username',
                'label' => 'Работник',
                'value' => function($model){
                    return Html::a($model->employee->user->username, ['employee/view', 'id' => $model->employee->id]);
                },
                'format' => 'html',
            ],
            [
                'attribute' => 'organisation.name',
                'label' => 'Организация',
                'value' => function($model){
                    return Html::a($model->organisation->name, ['organisation/view', 'id' => $model->organisation->id]);
                },
                'format' => 'html',
            ],
            'date_start',
            'date_end',
            [
                'attribute' => 'is_finished',
                'label' => 'Завершено',
                'value' => function($model){
                    return $model->is_finished ?
                        Html::tag('span', 'Да', ['class' => 'label label-success']) :
                        Html::tag('span', 'Нет', ['class' => 'label label-danger']);
                },
                'format' => 'html'
            ],
            ['class' => 'yii\grid\ActionColumn',
                'template'=>'{consumption} {update} {delete}',
                'buttons'=>[
                    'consumption' => function ($url, $model) {
                        return Html::a('<span class="fa fa-table"></span>', Url::to(['voyage/consumption', 'voyage_id' => $model->id]));
                    }
                ]
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
