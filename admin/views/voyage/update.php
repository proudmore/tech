<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use admin\widgets\switchbtn\SwitchBtn;
use kartik\select2\Select2;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model app\models\Voyage */

$this->title = Yii::t('app', 'Обновить {modelClass}: ', [
    'modelClass' => 'Командировка',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Командировки'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="voyage-update">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'employee_id')->textInput(['disabled' => true, 'value' => $model->employee->user->username]) ?>

    <?= $form->field($model, 'organisation_id')->textInput(['disabled' => true, 'value' => $model->organisation->name]) ?>

    <?= $form->field($model, 'date_start')->widget(DatePicker::className(), [
        'removeButton' => false,
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'yyyy-mm-dd',
            'startDate' => date('Y-m-d')
        ]
    ])  ?>

    <?= $form->field($model, 'date_end')->widget(DatePicker::className(), [
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'yyyy-mm-dd',
        ]
    ]) ?>

    <?= $form->field($model, 'is_finished')->widget(SwitchBtn::className(), [
        'options' => [
            'onText' => 'Завершено',
            'offText' => '',
        ],
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
