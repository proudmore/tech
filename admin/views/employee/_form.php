<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use admin\widgets\SwitchBtn;
use kartik\select2\Select2;
use \yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Employee */
/* @var $user \dektrium\user\models\User */
/* @var $form yii\widgets\ActiveForm */
/* @var $departments \app\models\Department[] */

?>

<div class="employee-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($user, 'username')->textInput() ?>

    <?= $form->field($user, 'password')->passwordInput() ?>

    <?= $form->field($user, 'email')->textInput() ?>

    <?= $form->field($model, 'department_id')->widget(Select2::className(), [
            'data' => ArrayHelper::map($departments, 'id', 'name')
    ]) ?>

    <?= $form->field($model, 'is_free')->widget(SwitchBtn::className(), [
        'options' => [
            'onText' => 'Свободен',
            'offText' => '',
        ],
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
