<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use admin\widgets\SwitchBtn;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Employee */
/* @var $user \dektrium\user\models\User */

$this->title = 'Create Employee';
$this->params['breadcrumbs'][] = ['label' => 'Employees', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employee-create">


    <div class="employee-form">

        <?php $form = ActiveForm::begin([
//                'validationUrl' => \yii\helpers\Url::to(['employee/create-validate']),
//                'id' => 'registration-form',
//                'enableAjaxValidation' => true
        ]); ?>

        <?= $form->field($user, 'username')->textInput() ?>

        <?= $form->field($user, 'password')->passwordInput() ?>

        <?= $form->field($user, 'email')->textInput() ?>

        <?= $form->field($model, 'department_id')->widget(Select2::className(), [
            'data' => ArrayHelper::map($departments, 'id', 'name')
        ]) ?>

        <?= $form->field($model, 'is_free')->widget(SwitchBtn::className(), [
            'options' => [
                'onText' => 'Свободен',
                'offText' => '',
            ],
        ]) ?>

        <div class="form-group">
            <?= Html::submitButton('Создать', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>



</div>
