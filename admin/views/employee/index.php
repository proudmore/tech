<?php

use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Html;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Работники';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employee-index">

    <p>
        <a class="btn btn-success" href="<?= Url::to(['/employee/create']) ?>"><span class="glyphicon glyphicon-plus"></span></a>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [

            'id',
            [
                'attribute' => 'user',
                'value' => 'user.username',
                'label' => 'Работник'
            ],
            'department_id',
            [
                'attribute' => 'is_free',
                'value' => function($model){
                    return $model->is_free ?
                        Html::tag('span', 'Да', ['class' => 'label label-success']) :
                        Html::tag('span', 'Нет', ['class' => 'label label-danger']);
                },
                'format' => 'html'

            ],


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
