<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Organisation */

$this->title = 'Create Organisation';
$this->params['breadcrumbs'][] = ['label' => 'Organisations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="organisation-create">


    <?= $this->render('_form', [
        'model' => $model,
        'cities' => $cities
    ]) ?>

</div>
