<?php
/**
 * Created by PhpStorm.
 * User: vetrinus
 * Date: 13.05.17
 * Time: 2:53
 */

namespace admin\modules\api\controllers;

use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;
use app\models\Voyage;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;

class VoyageController extends ActiveController
{
    public $modelClass = 'app\models\Voyage';


    public function actions()
    {
        $actions = parent::actions();
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];
        return $actions;
    }

    public function prepareDataProvider()
    {
        return new ActiveDataProvider(['query' => Voyage::find(), 'pagination' => false]);
    }

    public function actionUpdate($id){
        $model = $this->findModel($id);

        $model->load(\Yii::$app->getRequest()->getBodyParams(), '');
        if ($model->save() === false && !$model->hasErrors()) {
            throw new ServerErrorHttpException('Failed to update the object for unknown reason.');
        }
        return $model;
    }

    public function findModel($id){
        $model = Voyage::findOne(['id' => $id]);
        if($model == null){
            throw new NotFoundHttpException();
        } else {
            return $model;
        }
    }
}