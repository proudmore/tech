<?php
/**
 * Created by PhpStorm.
 * User: vetrinus
 * Date: 09.05.17
 * Time: 20:46
 */

namespace admin\modules\api\controllers;


use app\models\Consumption;
use yii\base\InvalidParamException;
use yii\data\ActiveDataProvider;
use yii\rest\ActiveController;

class ConsumptionController extends ActiveController
{
    public $modelClass = 'app\models\Consumption';

    public function actions()
    {
        $actions = parent::actions();
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];
        return $actions;
    }

    public function prepareDataProvider()
    {
        return new ActiveDataProvider(['query' => Consumption::find(), 'pagination' => false]);
    }

    public function actionUpdate(){

    }

    public function actionDelete(){

    }
}