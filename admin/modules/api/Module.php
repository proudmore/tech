<?php

namespace admin\modules\api;

use common\models\User;
use dektrium\user\helpers\Password;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

/**
 * api module definition class
 */
class Module extends \yii\base\Module
{

    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'admin\modules\api\controllers';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        //unset($behaviors['authenticator']);
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => ['http://employee.tech.local'],
                'Access-Control-Request-Method' => ['*'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => true,
            ],
        ];
//        $behaviors['verbFilter'] = [
//            'class' => VerbFilter::className(),
//            'actions' => [
//                '*' => ['OPTIONS']
//            ],
//        ];
//        $behaviors['authenticator'] = [
//            'class' => HttpBasicAuth::className(),
//            'auth' => [$this, 'auth'],
//            'except' => ['/voyages/options'],
//        ];
        return $behaviors;


//
//        $behaviors = parent::behaviors();
//
//        // remove authentication filter
//        $auth = $behaviors['authenticator'] = [
//            'class' => HttpBasicAuth::className(),
//            'auth' => [$this, 'auth'],
//            'except' => ['/voyages/options'],
//        ];
//        unset($behaviors['authenticator']);
//
//        // add CORS filter
//        $behaviors['corsFilter'] = [
//            'class' => \yii\filters\Cors::className(),
//            'cors' => [
//                'Origin' => ['http://employee.tech.local'],
//                'Access-Control-Request-Method' => ['*'],
//                'Access-Control-Request-Headers' => ['*'],
//                'Access-Control-Allow-Credentials' => true,
//            ]
//        ];
//
//        // re-add authentication filter
//        $behaviors['authenticator'] = $auth;
//        // avoid authentication on CORS-pre-flight requests (HTTP OPTIONS method)
//        $behaviors['authenticator']['except'] = ['options'];
//        $behaviors['authenticator']['auth'] = [$this, 'auth'];
//
//        return $behaviors;
    }


    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        \Yii::$app->user->enableSession = false;
        \Yii::$app->errorHandler->errorAction = null;
        //\Yii::$app->response->format = Response::FORMAT_HTML;
    }

    public function auth($username, $password)
    {
        $user = User::findOne(['username' => $username]);

        if(Password::validate($password, Password::hash($password))){
            return $user;
        } else {
            throw new ForbiddenHttpException('Forbidden');
        }


    }


}
