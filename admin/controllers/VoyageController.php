<?php

namespace admin\controllers;

use app\models\Consumption;
use app\models\Employee;
use app\models\Organisation;
use Yii;
use app\models\Voyage;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * VoyageController implements the CRUD actions for Voyage model.
 */
class VoyageController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    /**
     * Lists all Voyage models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Voyage::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Voyage model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Voyage model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Voyage();

        $employees = Employee::getFreeEmployers();

        $users = [];
        foreach ($employees as $employee){
            $tmp['id'] = $employee->id;
            $tmp['name'] = $employee->user->username;
            $users[] = $tmp;
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->employee->setBusy();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'users' => $users,
                'organisations' => Organisation::find()->asArray()->all()
            ]);
        }
    }

    /**
     * Updates an existing Voyage model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $users = [];



        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'organisations' => Organisation::find()->asArray()->all(),
                'users' => $users
            ]);
        }
    }

    /**
     * Deletes an existing Voyage model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Voyage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Voyage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Voyage::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionConsumption($voyage_id){

        $dataProvider = new ActiveDataProvider([
            'query' => Consumption::find()->where(['voyage_id' => $voyage_id]),
        ]);

        return $this->render('consumption', [
            'dataProvider' => $dataProvider,
            'voyage_id' => $voyage_id
        ]);
    }
}
