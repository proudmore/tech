<?php
return [
    ['label' => 'Организации', 'url' => '/organisation', 'roles' => ['admin']],
    ['label' => 'Отделы', 'url' => '/department', 'roles' => ['admin']],
    ['label' => 'Работники', 'url' => '/employee', 'roles' => ['admin']],
    ['label' => 'Города', 'url' => '/city', 'roles' => ['admin']],
    ['label' => 'Командировки', 'url' => '#', 'items' => [
        ['label' => 'Создать', 'url' => '/voyage/create', 'roles' => ['admin']],
        ['label' => 'Список', 'url' => '/voyage', 'roles' => ['admin']]
    ], 'roles' => ['admin']]
];