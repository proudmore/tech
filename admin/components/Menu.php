<?php

namespace admin\components;


abstract class Menu extends \yii\base\Object {

    /**
     * @param string $menuConfig Menu config path
     * @return array Built menu array
     */
    static public function build( $menuConfig = '' ) {
        $menu = require_once \Yii::getAlias($menuConfig);

        $roles = \Yii::$app->user->identity->roles;
        $permissions = \Yii::$app->user->identity->permissions;

        static::normalizeMenu( $menu, $roles, $permissions );

        return $menu;
    }

    protected static function normalizeMenu( &$menu, $roles, $permissions ) {
        foreach ( $menu as $key => $menuItem ) {
            if ( isset($menuItem['roles']) ) {
                $allowForRoles = false;
                $allowForPerm = false;

                if ( isset($menuItem['roles']) && !empty($menuItem['roles']) ) {
                    foreach ( $roles as $roleName => $roleObject ) {
                        if ( in_array($roleName, $menuItem['roles']) ) {
                            $allowForRoles = true;
                            break;
                        }
                    }
                } else {
                    $allowForRoles = true;
                }

                if ( isset($menuItem['permissions']) && !empty($menuItem['permissions']) ) {
                    foreach ( $permissions as $permName => $permObject ) {
                        if ( in_array($permName, $menuItem['permissions']) ) {
                            $allowForPerm = true;
                            break;
                        }
                    }
                } else {
                    $allowForPerm = true;
                }

                if ( !$allowForRoles || !$allowForPerm ) {
                    unset($menu[$key]);
                }
            } elseif ( isset($menuItem['items']) ) {
                static::normalizeMenu($menuItem['items'], $roles, $permissions);
            }
        }
    }

}
