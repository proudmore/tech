function createButtonComplete(id){
 return '<button type="button" class="btn btn-success btn-sm" onclick="complete(this)" voyage_id="' + id + '"><span class="glyphicon glyphicon-flag"></span></button>';
}

function createButtonConsumption(id){
    return '<a href="/site/consumption?voyage_id=' + id + '"><button type="button" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-list-alt"></span></button></a>';
}

function build_actions_column(item){
    var actions = document.createElement('td');
    if(item.is_finished == 0){
        actions.innerHTML =  createButtonConsumption(item.id) + ' ' + createButtonComplete(item.id);
    } else {
        actions.innerHTML =  createButtonConsumption(item.id);
    }

    return actions;
}

function release(data) {
    $('#login-block').hide();
    $('#content-block').show();
    var container = $('#tbl-container');
    data.forEach(function(item, i, arr) {
        var tr = document.createElement('tr');
        for (var key in item){
            if(key != 'consumptions' && key != '_links' && key != 'id'){
                var el = document.createElement('td');
                if(key == 'is_finished'){
                    if(item[key] == 0){
                        el.innerHTML = '<span class="label label-danger">Нет</span>';
                    } else {
                        el.innerHTML = '<span class="label label-success">Да</span>';
                    }
                } else {
                    el.textContent = item[key];
                }
                tr.appendChild(el);
            }


        }
        var actions = build_actions_column(item);
        tr.appendChild(actions);
        container.append(tr);
    });
}

function getData() {
    $.ajax({
        url: 'http://admin.tech.local/api/voyages',
        type: 'GET',
        success: function (data){
            release(data);
        }
    })
}

$(document).ready(function () {
    getData();
});


function complete(element){
    var id = element.getAttribute('voyage_id');
    $.ajax({
        url: 'http://admin.tech.local/api/voyages/' + id,
        type: 'PATCH',
        success: function (data){
            release(data);
        }
    })
}